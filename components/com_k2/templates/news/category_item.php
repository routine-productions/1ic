<?php
defined('_JEXEC') or die;
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
?>
<div class="News-Item">
    <div class="News-One-Images">
        <a href="/news<?= $this->item->link ?>">
            <img class="News-Photo" src="<?= $this->item->image ?>" alt="">
        </a>
        <span class="News-Date"><?= JHTML::_('date', $this->item->created, JText::_('d.m.y')) ?></span>
    </div>
    <h3 class="News-Title"><a href="/news<?= $this->item->link ?>"><?= $this->item->title ?></a>
    </h3>
</div>