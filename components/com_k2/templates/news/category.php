<section class="News wow fadeInUp" data-wow-offset="300" data-delay=".6s">
    <h2>Новости</h2>
    <div class="News-List">
        <?php
        foreach ($this->leading as $key => $item) {
            $this->item = $item;
            echo $this->loadTemplate('item');
        }
        ?>
    </div>
    <a href="/news" class="More">
        <span>Перейти к новостям</span>
    </a>
</section>