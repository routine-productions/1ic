<?php
defined('_JEXEC') or die;
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
?>
<section class="Post">
    <header>
        <h3 class="Post-Title"><?=$this->item->title?></h3>
        <p class="Post-Date"><?= JHTML::_('date', $this->item->created, JText::_('d.m.y')) ?></p>
    </header>
    <article class="Post-Content">
        <img class="Post-Photo" src="<?= $this->item->image ?>" alt="">

        <?=$this->item->introtext?>
        <?=$this->item->fulltext?>
    </article>
</section>