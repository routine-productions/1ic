<header class="Site-Header Small">
    <div class="Inner Node">
        <div class="Top">
            <a class="Company-Logotype" href="/">
                <img src="/img/ico-logo-top.png" alt="">
            </a>
            <nav class="Navigation">
                <ul>
                    <li><a href="/">Главная</a></li>
                    <li><a href="/about">О нас</a></li>
                    <li><a href="/news">Новости</a></li>
                    <li><a href="/contacts">Контакты</a></li>
                </ul>
            </nav>
            <a class="Callback" href="#">
                <img src="/img/ico-callback.png" alt="">
                <span>СВЯЗАТЬСЯ С НАМИ</span>
            </a>
        </div>
    </div>
</header>