<header class="Site-Header Big">
    <div class="Inner Node">
        <div class="Top">
            <nav class="Navigation">
                <ul>
                    <li><a href="/">Главная</a></li>
                    <li><a href="/about">О нас</a></li>
                    <li><a href="/news">Новости</a></li>
                    <li><a href="/contacts">Контакты</a></li>
                </ul>
            </nav>
            <a class="Callback" href="#">
                <img src="/img/ico-callback.png" alt="">
                <span>СВЯЗАТЬСЯ С НАМИ</span>
            </a>
        </div>

        <div class="Middle">
            <a class="Company-Logotype" href="/">
                <img src="/img/ico-logo-top.png" alt="">
            </a>
            <h1 class="Company-Name">
                Первый<br>
                Независимый<br>
                Телеканал
            </h1>
            <a href="/live" class="Watch-Air">Смотреть прямой эфир</a>
        </div>
    </div>
</header>