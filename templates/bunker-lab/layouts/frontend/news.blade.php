@extends('frontend._index')

@section('content')
    <section class="News">
        <h2>Новости</h2>

        <div class="News-List">
            <article class="News-Item">
                <div class="News-One-Images">
                    <a href="#">
                        <img class="News-Photo" src="/upload/pic-news-1.jpg" alt="">
                    </a>
                    <span class="News-Date">19.04.16</span>
                </div>
                <h3 class="News-Title"><a href="#">Зампред ЦИК высказала новую версию срыва выборов в Мариуполе</a></h3>
            </article>
            <article class="News-Item">
                <div class="News-One-Images">
                    <a href="#">
                        <img class="News-Photo" src="/upload/pic-news-2.jpg" alt="">
                    </a>
                    <span class="News-Date">19.04.16</span>
                </div>
                <h3 class="News-Title"><a href="#">Зампред ЦИК высказала новую версию срыва выборов в Мариуполе</a></h3>
            </article>
            <article class="News-Item">
                <div class="News-One-Images">
                    <a href="#">
                        <img class="News-Photo" src="/upload/pic-news-3.jpg" alt="">
                    </a>
                    <span class="News-Date">19.04.16</span>
                </div>
                <h3 class="News-Title"><a href="#">Зампред ЦИК высказала новую версию срыва выборов в Мариуполе</a></h3>
            </article>
            <article class="News-Item">
                <div class="News-One-Images">
                    <a href="#">
                        <img class="News-Photo" src="/upload/pic-news-2.jpg" alt="">
                    </a>
                    <span class="News-Date">19.04.16</span>
                </div>
                <h3 class="News-Title"><a href="#">Зампред ЦИК высказала новую версию срыва выборов в Мариуполе</a></h3>
            </article>
            <article class="News-Item">
                <div class="News-One-Images">
                    <a href="#">
                        <img class="News-Photo" src="/upload/pic-news-1.jpg" alt="">
                    </a>
                    <span class="News-Date">19.04.16</span>
                </div>
                <h3 class="News-Title"><a href="#">Зампред ЦИК высказала новую версию срыва выборов в Мариуполе</a></h3>
            </article>
            <article class="News-Item">
                <div class="News-One-Images">
                    <a href="#">
                        <img class="News-Photo" src="/upload/pic-news-3.jpg" alt="">
                    </a>
                    <span class="News-Date">19.04.16</span>
                </div>
                <h3 class="News-Title"><a href="#">Зампред ЦИК высказала новую версию срыва выборов в Мариуполе</a></h3>
            </article>            <div class="News-Item">
                <div class="News-One-Images">
                    <a href="#">
                        <img class="News-Photo" src="/upload/pic-news-3.jpg" alt="">
                    </a>
                    <span class="News-Date">19.04.16</span>
                </div>
                <h3 class="News-Title"><a href="#">Зампред ЦИК высказала новую версию срыва выборов в Мариуполе</a></h3>
            </div>
            <article class="News-Item">
                <div class="News-One-Images">
                    <a href="#">
                        <img class="News-Photo" src="/upload/pic-news-2.jpg" alt="">
                    </a>
                    <span class="News-Date">19.04.16</span>
                </div>
                <h3 class="News-Title"><a href="#">Зампред ЦИК высказала новую версию срыва выборов в Мариуполе</a></h3>
            </article>
            <article class="News-Item">
                <div class="News-One-Images">
                    <a href="#">
                        <img class="News-Photo" src="/upload/pic-news-1.jpg" alt="">
                    </a>
                    <span class="News-Date">19.04.16</span>
                </div>
                <h3 class="News-Title"><a href="#">Зампред ЦИК высказала новую версию срыва выборов в Мариуполе</a></h3>
            </article>

        </div>

    </section>
@endsection