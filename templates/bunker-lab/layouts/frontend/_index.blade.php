<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Первый независимый канал</title>

    <link rel="stylesheet" href="/css/init.css">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,500,700&subset=latin,cyrillic' rel='stylesheet'
          type='text/css'>

    <link rel="icon" href="/favicon.png">
    <link rel="apple-touch-icon" href="/img/ico-appletouchicon.png">

    <script src="/js/libs/wow.js"></script>
    <script src="/js/libs/jquery-2.1.4.min.js"></script>
    <script src="/js/libs/jquery.parallax.min.js"></script>
    <script src="/js/plugins/jquery.modal.js"></script>
    <script src="/js/index.js"></script>
</head>

<body id="Body">


<div class="Rectangulars" id="Rectangulars">

    <div class="Layer-1">
        <div class="Rectangular Rectangular-1 Size-1"></div>
        <div class="Rectangular Rectangular-2 Size-1"></div>
        <div class="Rectangular Rectangular-3 Size-1"></div>
        <div class="Rectangular Rectangular-4 Size-1"></div>
        <div class="Rectangular Rectangular-5 Size-1"></div>
    </div>

    <div class="Layer-2">
        <div class="Rectangular Rectangular-6 Size-2"></div>
        <div class="Rectangular Rectangular-7 Size-2"></div>
        <div class="Rectangular Rectangular-8 Size-2"></div>
        <div class="Rectangular Rectangular-9 Size-2"></div>
        <div class="Rectangular Rectangular-10 Size-2"></div>
    </div>

    <div class="Layer-3">
        <div class="Rectangular Rectangular-11 Size-3"></div>
        <div class="Rectangular Rectangular-12 Size-3"></div>
        <div class="Rectangular Rectangular-13 Size-3"></div>
        <div class="Rectangular Rectangular-14 Size-3"></div>
        <div class="Rectangular Rectangular-15 Size-3"></div>
    </div>


</div>

<div class="Wrapper">

    <div class="Modal-Outer">
        <div class="Modal">
            <a href="#" class="Close">×</a>

            <h3>Связаться с нами</h3>

            <form action="">
                <div class="Form-Row">
                    <label for="User-Name">Имя</label>
                    <input id="User-Name" name="name" type="text">
                </div>
                <div class="Form-Row">
                    <label for="User-Email">E&ndash;mail</label>
                    <input id="User-Email" name="email" type="text">
                </div>
                <div class="Form-Row">
                    <label for="User-Message">Сообщение</label>
                    <textarea id="User-Message" name="message" type="text"></textarea>
                </div>
                <button class="Send">Отправить</button>
                <div class="Message"></div>
            </form>
        </div>
    </div>

    <!-- Section Header start -->

    @if (Route::getCurrentRoute()->getName()=='frontend.home')
    @include('frontend.layouts.header_index')
    @else
    @include('frontend.layouts.header_inner')
    @endif
            <!-- Section Header end-->

    @yield('content')

            <!-- Section Footer start-->
    <footer class="Site-Footer wow fadeInUp" data-wow-offset="400">
        <div class="Footer-Top">
            <a href="/">
                <img class="Logo-Footer" src="/img/ico-footer-logo.png" alt="">
            </a>

            <div class="Footer-Top-Blocks vcard">
                <div class="Footer-Top-Block">
                    <h4 class="Footer-Block-Name">Адрес</h4>
                    <ul class="adr">
                        <li class="country-name">УКРАИНА</li>
                        <li><span class="postal-code">87515</span> <span class="locality">г. Мариуполь</span></li>
                        <li class="street-address">пр. Ленина 1</li>
                    </ul>
                </div>
                <div class="Footer-Top-Block">
                    <h4>Телефоны</h4>
                    <ul>
                        <li class="tel">+38 (067) 491–79–18</li>
                        <li class="tel">+38 (050) 609–40–11</li>
                        <li class="tel">+37 (25) 991–30–77</li>
                    </ul>
                </div>
                <div class="Footer-Top-Block">
                    <h4 class="Footer-Block-Name">почта</h4>
                    <a class="email" href="mailto:tv@1ic.biz">tv@1ic.biz</a>
                </div>
            </div>
        </div>
        <div class="Map">
            <script type="text/javascript" charset="utf-8"
                    src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=Rvs-mbE4Jj4e-VdItWhlfcsqRL7IUWzZ&width=100%&height=500&lang=ru_UA&sourceType=constructor"></script>
        </div>
        <p class="Last-Text">© Первый независимый телеканал 2015</p>
    </footer>
    <!-- Section Footer end-->
</div>


<script>
    new WOW().init();
</script>
</body>
</html>

