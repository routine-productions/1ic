@extends('frontend._index')

@section('content')
        <!-- Section Content start -->

<div class="Content-Home">

    <section class="Air">
        <div class="Node">
            <h2>Прямая трансляция</h2>

            <div class="Video-Air">
                <a href="#" class="Button-Play"></a>
                <p class="Air-Title">Смотреть прямой эфир</p>
            </div>
            <section class="Advantages">
                <div class="Advantages-Column">
                    <div class="wow fadeIn Advantage-Item" data-wow-delay=".2s">
                        <img src="/img/ico-advantage-1.png" alt="">
                        <dl>
                            <dt>100% Собственного контента</dt>
                            <dd>более 50 уникальных программ</dd>
                        </dl>
                    </div>
                    <div class="wow fadeIn Advantage-Item" data-wow-delay=".4s">
                        <img src="/img/ico-advantage-2.png" alt="">
                        <dl>
                            <dt>Прямые трансляции 24 часа в сутки</dt>
                            <dd>доля прямого эфира - 60%</dd>
                        </dl>
                    </div>
                    <div class="wow fadeIn Advantage-Item" data-wow-delay=".6s">
                        <img src="/img/ico-advantage-3.png" alt="">
                        <dl>
                            <dt>Народный телеканал</dt>
                            <dd>каждый зритель может участвовать <br>в создании контента</dd>
                        </dl>
                    </div>
                </div>
                <div class="Advantages-Column">
                    <div class="wow fadeIn Advantage-Item" data-wow-delay=".2s">
                        <img src="/img/ico-advantage-4.png" alt="">
                        <dl>
                            <dt>Просмотр в любой точке мира</dt>
                            <dd>через кабельные сети и Youtube</dd>
                        </dl>
                    </div>
                    <div class="wow fadeIn Advantage-Item" data-wow-delay=".4s">
                        <img src="/img/ico-advantage-5.png" alt="">
                        <dl>
                            <dt>Вещание в формате Full HD</dt>
                            <dd>запись и распространение контента в 4k</dd>
                        </dl>
                    </div>
                    <div class="wow fadeIn Advantage-Item" data-wow-delay=".6s">
                        <img src="/img/ico-advantage-6.png" alt="">
                        <dl>
                            <dt>Поддержка социальных проектов</dt>
                            <dd>мы всегда рады помочь нуждающимся</dd>
                        </dl>
                    </div>
                </div>
            </section>
        </div>

    </section>
</div>
@endsection