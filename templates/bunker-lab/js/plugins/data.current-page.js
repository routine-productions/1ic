/*
 * Copyright (c) 2015
 * Data Scripts - Current Page
 * Version 1.0.0
 * Create 2015.12.17
 * Author Bunker Labs

 * Usage:
 * work with tag <html>

 * /news/article#prologue
 * to
 * <html class="Page-News-Article Hash-Prologue">
 */
(function ($) {
    $(document).ready(function () {

        if (location.pathname != '/') {
            var Path = location.pathname.split('/').slice(1),
                Class = 'Page';

            for (var Index = 0; Index < Path.length; Index++) {
                Class += '-' + Path;
            }
            $('html').addClass(Class);
        } else {
            $('html').addClass('Page-Home');
        }

        if (location.hash) {
            $('html').addClass('Hash-' + location.hash.charAt(1).toUpperCase() + location.hash.slice(2));
        }
    });
})(jQuery);