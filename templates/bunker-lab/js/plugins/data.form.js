/*
 * Copyright (c) 2015
 * Data Scripts - Form
 * Version 1.0.1
 * Create 2015.12.03
 * Author Bunker Labs

 * Usage:
 * Add class name 'Script-Form'  - to mark Form
 * Add class name 'Script-Form-Button'  - to set Send Button
 * Add class name
 * Add attribute

 * Code structure:
 * <form class='Script-Form'
 *       data-form-email='admin@bunker-labs.com'
 *       data-form-subject='Order form website'
 *       data-form-url='path-to-your-script/data.form.php'
 *       data-form-method='POST'
 *       >
 *     <input type='text' name='name' data-input-title='Your first name'>
 *     <input class='Script-Form-Numeric' type='text' name='phone' data-input-title='Your telephone number'>
 *     <input class='Script-Form-Email' type='text' name='email' data-input-title='Your e-mail'>
 *     <textarea name='email' data-input-title='Your e-mail'></textarea>
 *     <input type='hidden' name='' value=''>
 *     <button class='Script-Form-Button'></button>
 *     <div class="Script-Form-Result"></div>
 * </form>
 *
 * todo: default actions success and error
 * todo: required fields
 * todo: validation
 * todo: checkboxes inputs support
 * todo: radio inputs support
 */
(function ($) {
    $(document).ready(function () {
        var $Button = $('.Script-Form-Button'),
            Forms = '.Script-Form',
            $Result = $('.Script-Form-Result'),
            Input = 'textarea,input,select',
            Validate_Numeric = $('.Script-Form-Numeric'),
            Validate_Email = $('.Script-Form-Email'),
            Message_Error = 'Заполните все поля!';

        $Button.click(function () {
            var Data = {},
                Form = $(this).parents(Forms),
                Url = Form.attr('data-form-url'),
                Method = Form.attr('data-form-method'),
                Valid = true;

            // Validate
            var Numeric = $(Validate_Numeric, Form).val(),
                Email_Exp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                Email = $(Validate_Email, Form).val();

            if (Numeric && !$.isNumeric(Numeric)) {
                Valid = Message_Error;
            } else if (Email && !Email_Exp.test(Email)) {
                Valid = Message_Error;
            }


            // Collect Data
            Data.input = {};
            Data.email = Form.attr('data-form-email');
            Data.subject = Form.attr('data-form-subject');

            $(Input, Form).each(function () {

                var Title = $(this).attr('data-input-title'),
                    Value = $(this).val(),
                    Name_Attr = $(this).attr('name');

                if (Title && Value) {
                    Data.input[Name_Attr] = {};
                    Data.input[Name_Attr].title = Title;
                    Data.input[Name_Attr].value = Value;
                } else {
                    Valid = Message_Error;
                    $(this).addClass('Script-Message-Error');
                }
            });

            // Send Form
            if (Valid == true) {
                $.ajax({
                    url: Form.attr('data-form-url') ? Form.attr('data-form-url') : 'data.form.php',
                    type: Form.attr('data-form-method') ? Form.attr('data-form-method') : 'POST',
                    data: Data,

                    success: function (Data) {
                        $Result.text(Data);
                        $(Input, Form).val('');
                    },
                    error: function (Data) {
                        $Result.text(Data);
                    }
                });
            } else {
                $Result.text(Valid);
            }
        });

        $(Input).click(function(){
            $(this).removeClass('Script-Message-Error');
        });
    });
})(jQuery);