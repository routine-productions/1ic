(function ($) {
    $(document).ready(function () {
        var $Modal_Outer = $('.Modal-Outer'),
            $Modal_Window = $('.Modal'),
            $Close_Item = $('.Modal-Outer, .Close'),
            $Activator = $('.Callback');


        $Activator.click(function () {
            $Modal_Outer.fadeIn();
            return false;
        });

        $Close_Item.click(function () {
            $Modal_Outer.fadeOut();
        });

        $Modal_Window.click(function () {
            return false;
        });
    });
})(jQuery);