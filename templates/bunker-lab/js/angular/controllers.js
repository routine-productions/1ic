'use strict';

/* Controllers */

Backend_App.controller ( 'Pages_List_Ctrl', [ '$scope', '$http', function ( $scope, $http ) {

    $scope.Error = '';
    $http.post ( '/backend/pages' ).success ( function ( Data ) {
        $scope.Pages = Data;
    }
    ).error ( function ( Data ) {
        $scope.Error = 'Load Error';
    }
    );

    $scope.Order = 'title';

} ]
);
