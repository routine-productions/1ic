$(document).ready(function () {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Active Item
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $('a').each(function () {
        if ($(this).attr('href') == location.pathname) {
            $(this).addClass('Active');
        }
    });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Paralax
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $('.Rectangulars').delay(1000).fadeIn(1400);

    $(window).scroll(function () {
        var Scroll_Percent = Math.sqrt(( $(window).scrollTop() / $('body').height()) * 100);
        $('.Layer-1 .Rectangular').css('margin-top', '-' + (Scroll_Percent * 5) + '%');
        $('.Layer-2 .Rectangular').css('margin-top', '-' + (Scroll_Percent * 4) + '%');
        $('.Layer-3 .Rectangular').css('margin-top', '-' + (Scroll_Percent * 3) + '%');
    });

    $(document).on("mousemove", function (event) {
        var Position_X = event.pageX - ($(window).width() / 2);

        $('.Layer-1 div').css({
            'margin-left': -Position_X / 30
        });

        $('.Layer-2 div').css({
            'margin-left': -Position_X / 20
        });

        $('.Layer-3 div').css({
            'margin-left': -Position_X / 10
        });
    });


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Paralax
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $('.Button-Play,.Watch-Air').click(function () {
        $('.Video-Air').append('<iframe src="' + $('.You-Tube-Iframe iframe').attr('src') + '?autoplay=1" frameborder="0" allowfullscreen=""></iframe>');

        $('.Video-Air').delay(1100).animate({
            'width': '100%'
        }, 500).animate({
            'height': ($('.Node').width() * 9 / 16)
        }, 500);

        $('.Advantages').delay(600).animate({
            'left': '100%',
            'opacity': '0'
        }, 500, function () {
            $('.Advantages').hide();
        });

        $('iframe').delay(600).fadeIn(500);

        $('body,html').delay(100).animate({'scrollTop': $('.Air').position().top + 30}, 500);

        return false;
    });


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
});

$(document).ready(function () {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Active Item
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $('a').each(function () {
        if ($(this).attr('href') == location.pathname) {
            $(this).addClass('Active');
        }
    });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Paralax
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $('.Rectangulars').delay(1000).fadeIn(1400);

    $(window).scroll(function () {
        var Scroll_Percent = Math.sqrt(( $(window).scrollTop() / $('body').height()) * 100);
        $('.Layer-1 .Rectangular').css('margin-top', '-' + (Scroll_Percent * 5) + '%');
        $('.Layer-2 .Rectangular').css('margin-top', '-' + (Scroll_Percent * 4) + '%');
        $('.Layer-3 .Rectangular').css('margin-top', '-' + (Scroll_Percent * 3) + '%');
    });

    $(document).on("mousemove", function (event) {
        var Position_X = event.pageX - ($(window).width() / 2);

        $('.Layer-1 div').css({
            'margin-left': -Position_X / 30
        });

        $('.Layer-2 div').css({
            'margin-left': -Position_X / 20
        });

        $('.Layer-3 div').css({
            'margin-left': -Position_X / 10
        });
    });


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
});